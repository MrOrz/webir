#!/bin/bash

CACHE_URL='http://w.csie.org/~r01922036/cache.gzip'

# Compile tools
cd tools
mkdir bin
make clean
make
cd ../


# Download cache file
wget $CACHE_URL
tar xvf cache.gzip


# Create tmp for create-ngram
mkdir /tmp/r01922036
