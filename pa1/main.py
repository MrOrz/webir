# -*- coding: utf8 -*-

import argparse
import xml.etree.ElementTree as ET
import os
import marshal
import re
from operator import itemgetter
from pdb import set_trace
from subprocess import Popen, PIPE
from math import log, sqrt
from collections import defaultdict
from threading import Thread, Lock, BoundedSemaphore

print_lock = Lock()

#: Inverted index cache
INVERTED_CACHE_PATH = 'inverted_cache.marshal'

#: Stopword list
STOPWORDS = u'一不之也了了人他你個們在就我是有的而要說這都'
TRANS_TABLE = dict( (ord(i), ord(' ')) for i in STOPWORDS )
PUNC_MATCHER = re.compile(r'\W', flags=re.UNICODE)

#: Progress
PROGRESS_PERIOD = 100

#: Rocchio feedback parameters
ROCCHIO_ITERATION = 1   #: Number of iterations to execute if using relevance feedback
ROCCHIO_K = 20          #: Top-K results used in relevance feedback
ROCCHIO_ALPHA = 1.0     #: Weighting for the original query vector
ROCCHIO_BETA = 0.75     #: Weighting for the centroid of top-k vectors
#: Note: only positive feedback.


#: Global variables
args = {}     #: The exec arguments.
files = {}    #: Opened file descriptors.
pos_in_index = {}       #: Position of a word (unigram or bigram) in file-list.
df = {}                 #: document frequency of a word.
idf = {}                #: IDF of a word.
doc_vector_length = []  #: Document id (0~9xxxx) to length of its weight vector.


#: create-ngram
TMPDIR = '/tmp/r01922036'

def main():
  """Main program"""

  #: Populate the global variables
  global args, files, pos_in_index, df, idf, doc_vector_length
  args  = parse_args()
  files = open_files()
  (pos_in_index, df, idf, doc_vector_length) = read_inverted_index()
  idf = defaultdict(float, idf) #: Re-wrap the idf dictionary with default values.


  #: Load query vectors
  queries = read_queries(args.query)
  for query_id, query in queries.iteritems():
    print "Processing query " + query_id

    query_vector = Vector.create(query)

    iteration = ROCCHIO_ITERATION if args.relevance else 0

    #: Rocchio feedback iterations
    while True:
      #: Find related files and calculate similarity.
      related_files = calculate_doc_similarity_for(query_vector)
      print "%d documents found." % len(related_files)

      #: Now all related_files' similarity is calculated.
      #: Sort them using sorted()
      sorted_related_files = sorted(related_files.iteritems(), key=itemgetter(1), reverse=True)

      if iteration == 0:
        break

      #: Generating new query vector
      top_k = map(itemgetter(0), sorted_related_files[:ROCCHIO_K])
      query_vector = modifiy_query(query_vector, top_k)

      print "Rocchio iteration remains: ", iteration
      iteration -= 1

    #: Output
    outout_count = 0
    for file_id, similarity in sorted_related_files:
      if outout_count > 100: #: At most 100 documents
        break

      file_name = (read_document_path(file_id)[-15:]).lower()
      files['rank-list'].write("%s %s\n" % (query_id, file_name))
      outout_count += 1

    files['rank-list'].flush()

class Vector:
  """The vector (unigram+bigram)"""

  def __init__(self, unigram=[], bigram=[]):
    self.unigram = unigram  #: (wordId, tf-idf weight)
    self.bigram = bigram    #: (wordId1_wordId2, tf-idf weight)


  @classmethod
  def _merge_gram(cls, gram1, gram2):
    """
    Merge two sorted gram lists into a longer sorted list,
    Weight with the same word_id is summed together.
    """
    ret = []
    ptr1 = ptr2 = 0
    len1 = len(gram1)
    len2 = len(gram2)

    while ptr1 < len1 and ptr2 < len2:
      (word1, weight1) = gram1[ptr1]
      (word2, weight2) = gram2[ptr2]

      if word1 == word2:
        ret.append( (word1, weight1 + weight2) )
        ptr1 += 1
        ptr2 += 1
      elif word1 < word2:
        ret.append( gram1[ptr1] )
        ptr1 += 1
      else:
        ret.append( gram2[ptr2] )
        ptr2 += 1

    #: Concatenate the last part.
    ret += gram1[ptr1:] + gram2[ptr2:]

    return ret


  def __add__(self, another):
    """Vector addition"""

    return Vector( \
      unigram=Vector._merge_gram(self.unigram, another.unigram), \
      bigram=Vector._merge_gram(self.bigram, another.bigram) )


  def __mul__(self, weight):
    """Vector multiplication with numerical value"""

    return Vector(\
      unigram=map(lambda x: (x[0], weight*x[1]), self.unigram),
      bigram=map(lambda x: (x[0], weight*x[1]), self.bigram) )

  def __div__(self, divisor):
    """Vector division with numerical value"""

    return self.__mul__(1.0/divisor)


  @classmethod
  def create(cls, content):
    # set_trace()
    """Generate the Vector object from content string"""

    cmd = "tools/bin/create-ngram -tmp %s -vocab %s/vocab.all -n %d"

    def ngram(n):
      """Given n, execute cmd to get the ngram"""
      gram = []

      proc = Popen((cmd % (TMPDIR, args.model, n)), shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

      #: Remove punctuations and stopwords.
      #: Punctuations are converted to new lines so that the content do not become only 1 line
      #: and overflow create-ngram's line buffer.
      clean_content = PUNC_MATCHER.sub("\n", content).translate(TRANS_TABLE)
      # set_trace()

      (stdout, stderr) = proc.communicate(clean_content.encode('utf8'))
      for line in stdout.strip().split('\n'):
        (word_id, tf) = line.strip().split(' ')
        weight = int(tf) * idf[word_id]
        if weight > 0:
          gram.append( (word_id, weight) )

      return gram
    # set_trace()
    return cls(unigram=ngram(n=1), bigram=ngram(n=2))

def parse_args():
  """Parse ARGV"""

  parser = argparse.ArgumentParser()
  parser.add_argument('-r', dest='relevance', action='store_const', const=True, default=False)
  parser.add_argument('-i', dest='query', required=True)
  parser.add_argument('-o', dest='output', required=True)
  parser.add_argument('-m', dest='model', required=True)
  parser.add_argument('-d', dest='documents', required=True)

  return parser.parse_args()


def open_files():
  """Open files and return the file descriptors"""
  return {
    'inverted-index': open(args.model + '/inverted-index', 'rb'),
    'file-list': open(args.model + '/file-list', 'r'),
    'rank-list': open(args.output, 'w')
  }


def read_inverted_index():
  """
    Scan through the inverted index file and gather the following info:
    1. Position of a word in file-list.
    2. Document frequency of a word (so that we know how many lines to read)
    3. IDF of a word.
    4. sqrt( sum [(term weight)^2] ) of each document, where term weight = TF-IDF
       This is the vector length of the document.
  """

  print "Preprocessing inverted index file..."

  try:
    ret = marshal.load(open(INVERTED_CACHE_PATH, 'rb'))
    print "Loaded from cache."

  except IOError: #: Cache file not exist
    print "Cache not found. Rebuilding..."

    #: Files
    index_file = files['inverted-index']

    #: Get total number of documents (n)
    wc = Popen('wc -l %s/file-list'%args.model, shell=True, stdout=PIPE)

    total_doc_num = int( wc.stdout.read().strip().split(' ')[0] )
    log_total_doc_num = log(total_doc_num)

    #: The three kinds of information to collect
    file_pos = {}
    df = {}
    idf = {}
    squared_doc_lengths = [0] * total_doc_num #: Apply the square root later.

    #: Scanning through index file
    skip = 0
    current_idf = 0  #: Cached value
    while True:
      line = index_file.readline()

      if line == '': #: EOF
        break

      if skip > 0 :  #: Reading: doc_id, term_frequency
        (file_id, tf) = line.strip().split(' ')
        tfidf = int(tf) * current_idf
        squared_doc_lengths[int(file_id)] += tfidf * tfidf
        skip -= 1

      else: #: Reading: word_id1, word_id2, doc_frequency
        (word1, word2, skip) = line.split(' ')
        key = word1 if word2 == '-1' else word1 + '_' + word2
        df[key] = skip = int(skip)
        file_pos[key] = index_file.tell()
        if skip > 0:
          current_idf = idf[key] = log_total_doc_num - log(skip)

    ret = (file_pos, df, idf, map(sqrt, squared_doc_lengths))
    marshal.dump(ret, open(INVERTED_CACHE_PATH, 'wb'), 2)

    print "done."

  return ret

def read_queries(query_path):
  """Read the querie strings from the query file"""
  root = ET.parse(query_path).getroot()
  queries = {}

  for topic in root.iter('topic'):
    number = topic.find('number').text[-3:]
    text = ' '.join([
      topic.find('title').text,
      # topic.find('question').text[2:],
      # topic.find('narrative').text,
      topic.find('concepts').text
    ])

    queries[number] = text

  return queries

def read_document_path(file_id):
  """Read document file path from file ID"""
  line_num = file_id + 1
  ed = Popen('sed -n %dp %s/file-list'%(line_num, args.model), shell=True, stdout=PIPE)
  return ed.stdout.read().strip()[9:] #: Remove the leading "./CIRB010"

def read_document(file_id):
  """Read document from file_id and filelist and return its content"""

  file_name = read_document_path(file_id)

  doc = ET.parse(args.documents + '/' + file_name).getroot().find('doc')

  #: Read all paragraphs
  paragraphs = map(lambda x: x.text, list(doc.find('text')) )
  paragraphs.append(doc.find('title').text)

  return ' '.join( paragraphs )

def calculate_doc_similarity_for(query_vector):
  """Return file ids related to the query vector, and the similarities"""

  inverted_index_file = files['inverted-index']

  #: file_id --> partial inner product of weights
  related_files = defaultdict(float)


  #: Traversing each word in the query, finding related files.
  for word_id, query_weight in (query_vector.unigram + query_vector.bigram):

    #: Reading df lines in the inverted-index,
    #: starting from where the word is defined in inverted-index file.
    #: Each line read is used to populate related_files.
    inverted_index_file.seek(pos_in_index[word_id])
    current_idf = idf[word_id]
    for i in range(0, df[word_id]):
      line = inverted_index_file.readline()
      (file_id, tf) = line.split(' ')
      doc_weight = int(tf) * current_idf
      related_files[int(file_id)] += query_weight * doc_weight

  #: Convert inner products to similarities (by dividing with doc vector length)
  ret = {}
  for file_id, inner_prod in related_files.iteritems():
    ret[file_id] = inner_prod / doc_vector_length[file_id]

  return ret

def modifiy_query(old_q, top_k_file_ids):
  """
    Modify the query using Rocchio algorithms
    http://nlp.stanford.edu/IR-book/html/htmledition/the-rocchio71-algorithm-1.html
  """

  #: Calculate the centroid
  centroid = Vector()
  print "Extracting centroid of %d most related documents..." % len(top_k_file_ids)
  for file_id in top_k_file_ids:
    #: Read the full vectors
    content = read_document(file_id)
    centroid += Vector.create(content)

  centroid = centroid/len(top_k_file_ids)

  return old_q * ROCCHIO_ALPHA + centroid * ROCCHIO_BETA


  #: Calculate the centroid

#: Kick start
main()

# args = parse_args()
# (pos_in_index, df, idf, doc_vector_length) = read_inverted_index()
# idf = defaultdict(float, idf) #: Re-wrap the idf dictionary with default values.

# qm = modifiy_query(Vector(), [1, 2, 3, 4, 5, 6])
# set_trace()
