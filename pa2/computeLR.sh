#!/usr/bin/env python

import sys, itertools
from collections import defaultdict #: For idf map
from operator import itemgetter
from graph import Graph
from pdb import set_trace
from math import log, sqrt
from subprocess import Popen, PIPE

THRESHOLD = 0.1 #: Cosine threshold

if len(sys.argv) != 2 :
  print "Usage: ./computeLR <sentence_path>"
  sys.exit(1)

#: Reading IDF file, create word-idf mapping.
idf_file = open('idf', 'r')

#: Word --> IDF value, default to constant log(40252.0).
idf_map = defaultdict(itertools.repeat(log(40252.0)).next)
while True:
  line = idf_file.readline()

  if line == '': #: EOF
    break

  word, idf = line.strip().split(' ')
  idf_map[word] = float(idf)

#: Get sentence count in sentence file
wc = Popen('wc -l %s' % sys.argv[1], shell=True, stdout=PIPE)
sentence_count = int(  wc.stdout.read().strip().split(' ')[0] )

#: Create the sparse matrix
A = Graph(sentence_count)

# set_trace()

weights = [] #: Weight vectors of each sentence

#: Reading through the .sen file to get weight vectors of each sentence.
sen_file = open(sys.argv[1], 'r')
while True:
  line = sen_file.readline()

  if line == '': #: EOF
    break

  words = line.strip().split(' ')
  weight = defaultdict(float) #: Word --> TF-IDF weighting

  #: Calculate TF-IDF (the product equals to idf of a word added tf times)
  for word in words:
    weight[word] += idf_map[word]

  weights.append(weight)

sen_file.close()

#: Calculate the weight vector length
def length(a):
  """Find the length of the given weight vector."""
  return sqrt( reduce(lambda s, i: s + i*i, a.itervalues(), 0) )

lengths = [length(a) for a in weights] #: weight vector lengths


def similarity(i, j):
  """Find the cosine similarity of the two weight dictionaries."""
  inner_prod = 0
  a = weights[i]
  b = weights[j]

  for term, weight_a in a.iteritems():
    weight_b = b[term]
    inner_prod += weight_a * weight_b

  return inner_prod / (lengths[i] * lengths[j])

#: Find similar sentences and construct the graph.
for i in xrange(0, sentence_count-1):
  for j in xrange(i+1, sentence_count):
    if similarity(i, j) > THRESHOLD:

      #: The graph is undirected.
      A.add_path(i, j)
      A.add_path(j, i)

#: Execute PageRank algorithm on the graph A.
lr = A.pagerank()

#: Sort the list of (id, lexrank)
lr_sorted = [(i, lr[i]) for i in xrange(0, sentence_count)]
lr_sorted = sorted(lr_sorted, key=itemgetter(1), reverse=True)

#: Output the pagerank file.
lr_file = open(sys.argv[1]+'.lexrank', 'w')
lr_file.write('%d\n' % sentence_count)
for id, lexrank in lr_sorted:
  lr_file.write('%d:%f\n' % (id+1, lexrank))

lr_file.close()