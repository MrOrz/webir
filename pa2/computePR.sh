#!/usr/bin/env python

import sys
from graph import Graph
from pdb import set_trace

if len(sys.argv) != 2 :
  print "Usage: ./computePR <graph_path>"
  sys.exit(1)

graph_file = open(sys.argv[1], 'r')

#: Get maxnode ID from the first line of the graph file.
maxnode = int(graph_file.readline().strip().split(' ')[1])

#: Create the sparse matrix
A = Graph(maxnode)

# set_trace()

#: Reading through the .graph file
while True:
  line = graph_file.readline()

  if line == '': #: EOF
    break

  node_id, out_node_list = line.strip().split(':')
  node_id = int(node_id) - 1
  out_nodes = map(lambda n: int(n) - 1, out_node_list.split(' ')[1:])

  for out_node_id in out_nodes:
    A.add_path(from_node=node_id, to_node=out_node_id)

graph_file.close()

#: Execute PageRank algorithm on the graph A.
pr = A.pagerank()

#: Output the pagerank file.
pr_file = open(sys.argv[1]+'.pagerank', 'w')
for i in xrange(0, len(pr)):
  pr_file.write('%d:%f\n' % (i+1, pr[i]))

pr_file.close()