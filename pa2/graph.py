from math import sqrt
from pdb import set_trace

DAMPING_FACTOR = 0.85
EPSILON2 = 1e-12 #: square of epsilon

class Graph:
  def __init__(self, s):
    """Initialize a sparse graph with size s"""
    self.inlinks = [[] for i in xrange(0, s)]
    self.outlink_count = [0.0] * s

  def add_path(self, from_node, to_node):
    """Add a link between the two ID values. ID should start from 0."""
    self.inlinks[to_node].append(from_node)
    self.outlink_count[from_node] += 1

  def pagerank(self):
    # set_trace()
    #: Initialization

    size = len(self.inlinks) #: graph node number
    last_pr = [1] * size     #: init page rank value

    #: Finding dangling node.
    dangling_nodes = []
    for i in xrange(0, size):
      if self.outlink_count[i] == 0.0:
        dangling_nodes.append(i)

    def calc_pr(node_id):
      """Calculate the new PR value for a certain node_id"""

      inlink_ids = self.inlinks[node_id]
      s = reduce( lambda s, i: s + last_pr[i]/self.outlink_count[i], inlink_ids, 0.0)

      return 1 - DAMPING_FACTOR + DAMPING_FACTOR * s

    #: Power iteration
    while True:
      #: Calculate the constant term contributed by dangling pages
      dangling_pr_sum = reduce(lambda s, i: s + last_pr[i], dangling_nodes, 0.0)
      dangling_term = DAMPING_FACTOR * dangling_pr_sum / size

      #: Calculate PR value
      pr = [calc_pr(n) + dangling_term for n in xrange(0, size)]

      #: Calculate distance
      square_dist = 0.0
      for i in xrange(0, size):
        diff = pr[i] - last_pr[i]
        square_dist += diff * diff

      #: Update last_pr
      last_pr = pr

      #: Check whether distance is small enough
      if square_dist < EPSILON2:
        return pr
      else:
        print "Square distance:", square_dist , ">", EPSILON2

